#!/usr/bin/python

import sys

def usage():
    print '+------------------------------------------------------------+'
    print '|                 Generic replacer by tags                   |'
    print '|                         | BSC |                            |'
    print '| Usage:                                                     |'
    print '|       ./command *.template1 *.template2 ... *.input        |'
    print '|                                                            |'
    print '|       The tags must be fo the type $Pn$, for example $P1$. |'
    print '|       The parameters must be in an *.input file,  with the |'
    print '|       following format:                                    |'
    print '|                                                            |'
    print '|         "* <name> <tag> = <value> "                        |'
    print '|                                                            |'
    print '|       For example:                                         |'
    print '|                                                            |'
    print '|         "* kinematic_viscosity P9 = 0.45"                  |'
    print '|                                                            |'
    print '|       The templates where the parameters will be replaced  |'
    print '|       must be *.template files.                            |'
    print '|                                                            |'
    print '|                                                            |'
    print '|                                         Ultra beta version.|'
    print '|                  Alfonso Santiago - alfonso.santiago@bsc.es|'
    print '+------------------------------------------------------------+'
    sys.exit()


if(len(sys.argv)< 2):
    usage()
elif(sys.argv[1]=='-v'):
    usage()

param =[0 for x in xrange(100)]
fr = []
fw = []

args= sys.argv

n=0
f=0

#APERTURA DE ARCHIVOS
for arg in args:
    if(arg.endswith('input')):
         fi = open(args[n], 'r')

    elif(arg.endswith('py')):
        flagPy = True

    elif(arg.endswith('template')):
        fr.append(open(args[n], 'r'))
        escritura=args[n].replace('.template','')
        fw.append(open(escritura, 'w'))
        f=f+1

    n=n+1
##

#LECTURA DE ARCHIVO INPUT
for line in fi:
    if (line.startswith('*')):
        line = line.strip()
        line = line.split()
        pnum=line[1].replace('P','')
        param[int(pnum)] = line[3]
    else:
        flagcomment = True

#LECTURA DE TEMPLATES Y ESCRITURA DE FINALES


for x in xrange(f):
    for line in fr[x]:
        tofile=line
        for n in xrange(100):
            substr= '$P'+str(n)+'$'
            if substr in line:
#               fw[x].write(line.replace( substr , 'BLABLA'))
                tofile=tofile.replace(substr, str(param[n]) )
        fw[x].write(tofile)
